import {StyleSheet} from 'react-native';
import Colors from './Colors';
import {sizeFont, sizeWidth} from '../src/ultils/SizeUltils';

const AppStyles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  row: {
    flexDirection: 'row',
  },
  m_left_10: {
    marginLeft: sizeWidth(10),
  },
  m_horizontal_10: {
    marginHorizontal: sizeWidth(10),
  },
  font_12: {
    fontSize: sizeFont(12),
  },
  text_input: {
    fontSize: sizeFont(12),
    color: Colors.grey70,
  },
  bg_white: {
    backgroundColor: Colors.white,
  },
  bg_red: {
    backgroundColor: Colors.red,
  },
  w_100: {
    width: 100,
  },
  mg_top_20: {
    marginTop: sizeWidth(20),
  },
  pd_horizontal_10: {
    paddingHorizontal: sizeWidth(10),
  },
  pd_vertical_10: {
    paddingVertical: sizeWidth(10),
  },
});

export default AppStyles;
