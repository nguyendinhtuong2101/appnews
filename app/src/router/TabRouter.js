import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import NewsScreen from '../screens/News';
import VideosScreen from '../screens/Videos';
import ExtensionsScreen from '../screens/Extensions';
import ProfileScreen from '../screens/Profile';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {Colors} from '../../res';
import {sizeFont} from '../ultils/SizeUltils';

const Tab = createBottomTabNavigator();

const BottomTabs = () => {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({focused}) => {
          let iconName, iconColor;

          if (route.name === 'Tin tức') {
            iconName = focused ? 'newspaper' : 'newspaper-outline';
          } else if (route.name === 'Video') {
            iconName = focused ? 'videocam' : 'videocam-outline';
          } else if (route.name === 'Tiện ích') {
            iconName = focused ? 'apps' : 'apps-outline';
          } else if (route.name === 'Cá nhân') {
            iconName = focused ? 'person-circle' : 'person-circle-outline';
          }

          iconColor = focused ? Colors.red : Colors.grey54;

          return <Ionicons name={iconName} size={25} color={iconColor} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: Colors.red,
        inactiveTintColor: Colors.grey54,
        labelStyle: {
          fontSize: sizeFont(12),
        },
        tabStyle: {
          backgroundColor: Colors.grey93,
          height: 50,
        },
        style: {
          height: 50,
        },
        keyboardHidesTabBar: true,
      }}>
      <Tab.Screen name={'Tin tức'} component={NewsScreen} />
      <Tab.Screen name={'Video'} component={VideosScreen} />
      <Tab.Screen name={'Tiện ích'} component={ExtensionsScreen} />
      <Tab.Screen name={'Cá nhân'} component={ProfileScreen} />
    </Tab.Navigator>
  );
};

export default BottomTabs;
