import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import BottomTabs from './TabRouter';

const Stack = createStackNavigator();

const AppRouter = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name={'Home'} component={BottomTabs} />
    </Stack.Navigator>
  );
};

export default AppRouter;
