import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {sizeFont, sizeWidth} from '../../ultils/SizeUltils';
import FetchImage from '../../components/common/FetchImage';
import {AppStyles, Colors} from '../../../res';
import TextNormal from '../../components/common/TextNormal';

const dataExtensions = [
  {
    title: 'Thời Tiết',
    icon_url:
      'https://cdn3.iconfinder.com/data/icons/luchesa-vol-9/128/Weather-512.png',
  },
  {
    title: 'Xổ Số',
    icon_url:
      'https://cdn0.iconfinder.com/data/icons/casino-and-gambling-flat/50/Casino__Gambling_-_Flat-18-256.png',
  },
  {
    title: 'Tỷ Giá',
    icon_url:
      'https://cdn1.iconfinder.com/data/icons/economic-crisis-5/64/Exchange_Rate-financial-money-trading-currency-yen-256.png',
  },
  {
    title: 'Phong Tục Việt Nam',
    icon_url:
      'https://cdn3.iconfinder.com/data/icons/nature-emoji/50/Leaf-512.png',
  },
  {
    title: 'Lịch Việt',
    icon_url:
      'https://cdn0.iconfinder.com/data/icons/merry-christmas-41/512/38-calendar-christmas-xmas-512.png',
  },
  {
    title: 'Lễ Hội Việt Nam',
    icon_url:
      'https://cdn1.iconfinder.com/data/icons/easter-10/32/fireworks-boom-bang-festival-celebration-new_year-stars-512.png',
  },
  {
    title: 'Tử Vi Cung Hoàng Đạo',
    icon_url:
      'https://cdn2.iconfinder.com/data/icons/zodiac-signs-2/128/Scorpio-512.png',
  },
  {
    title: 'Tử Vi',
    icon_url:
      'https://cdn4.iconfinder.com/data/icons/fat-cute-animal-chinese-zodiac-horoscope/256/Dragon_chinese_zodiac-01-512.png',
  },
  {
    title: 'Danh Ngôn Thế Giới',
    icon_url:
      'https://cdn4.iconfinder.com/data/icons/common-app-symbols-round-colored/1024/quote_quotation_paraphrase_quotations_app_round_colored-512.png',
  },
  {
    title: 'Xem Sao Giải Hạn',
    icon_url:
      'https://cdn1.iconfinder.com/data/icons/macster/70/.svg-17-512.png',
  },
  {
    title: 'Văn Khấn',
    icon_url:
      'https://cdn3.iconfinder.com/data/icons/education-209/64/book-note-paper-school-512.png',
  },
  {
    title: 'Xông Đất',
    icon_url:
      'https://cdn0.iconfinder.com/data/icons/halloween-2391/64/broom-512.png',
  },
];

const Extensions = () => {
  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={dataExtensions}
        renderItem={(item) => {
          return (
            <TouchableOpacity
              style={styles.containerExtension}
              activeOpacity={0.7}>
              <View style={styles.containerItem}>
                <FetchImage
                  uri={item.item.icon_url}
                  style={styles.containerImage}
                />
              </View>
              <TextNormal
                style={styles.textTitle}
                multiline={true}
                numberOfLines={2}>
                {item.item.title}
              </TextNormal>
            </TouchableOpacity>
          );
        }}
        keyExtractor={(item, index) => index.toString()}
        numColumns={3}
        columnWrapperStyle={[
          AppStyles.pd_horizontal_10,
          AppStyles.pd_vertical_10,
        ]}
        scrollEnabled={false}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  containerExtension: {
    flex: 1,
    alignItems: 'center',
    marginTop: sizeWidth(3),
  },
  containerItem: {
    width: sizeWidth(100),
    height: sizeWidth(100),
    backgroundColor: Colors.grey89,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: sizeWidth(5),
  },
  containerImage: {
    width: sizeWidth(50),
    height: sizeWidth(50),
  },
  textTitle: {
    width: sizeWidth(100),
    marginTop: sizeWidth(5),
    textAlign: 'center',
    fontSize: sizeFont(16),
  },
});

export default Extensions;
