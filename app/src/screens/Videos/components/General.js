import React, {useState, useEffect} from 'react';
import {View, StyleSheet, FlatList, TouchableOpacity} from 'react-native';
import {getNews} from '../../../api/Api';
import {sizeFont, sizeHeight, sizeWidth} from '../../../ultils/SizeUltils';
import FetchImage from '../../../components/common/FetchImage';
import TextNormal from '../../../components/common/TextNormal';
import {Colors} from '../../../../res';
import {fromNow} from '../../../ultils/Helper';
import {connect} from 'react-redux';
import {hideLoading, showLoading} from '../../../redux/actions/LoadingActions';
import SkeletonGeneral from '../../../components/shared/SkeletonGeneral';

const General = (props) => {
  const [listNews, setListNews] = useState([]);

  useEffect(() => {
    handleGetVideos();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleGetVideos = async () => {
    props.showLoading();
    try {
      let response = await getNews();
      setListNews(response);
    } catch (e) {
      console.log('error', e);
    }
    props.hideLoading();
  };

  const renderItem = (item) => {
    return (
      <View style={styles.containerItem}>
        <TouchableOpacity activeOpacity={0.7} style={[styles.containerButton]}>
          <FetchImage
            uri={
              'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSGPcdiyYbAZluOhQlHIbtaOdXORZMICoZqrg&usqp=CAU'
            }
            style={styles.containerImage}
          />
          <TextNormal
            style={styles.textTitle}
            multiline={true}
            numberOfLines={3}>
            {item.item.title}
          </TextNormal>
        </TouchableOpacity>
        <View style={styles.itemSource}>
          <TextNormal>{item.item.source}</TextNormal>
          <View style={styles.dot} />
          <TextNormal>{fromNow(item.item.published_at)}</TextNormal>
        </View>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      {props.loading && <SkeletonGeneral />}
      <FlatList
        data={listNews.data}
        renderItem={renderItem}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  containerItem: {
    height: sizeHeight(300),
    paddingHorizontal: sizeWidth(10),
    marginBottom: sizeWidth(10),
  },
  containerButton: {
    width: sizeWidth(355),
    height: sizeWidth(230),
  },
  containerImage: {
    width: sizeWidth(355),
    height: sizeWidth(180),
    borderRadius: sizeWidth(5),
  },
  textTitle: {
    fontSize: sizeFont(18),
    color: Colors.grey7,
    fontWeight: '700',
  },
  itemSource: {
    marginTop: sizeWidth(45),
    flexDirection: 'row',
    alignItems: 'center',
  },
  dot: {
    width: sizeWidth(4),
    height: sizeWidth(4),
    borderRadius: sizeWidth(2),
    backgroundColor: Colors.grey70,
    marginHorizontal: sizeWidth(5),
  },
});

export default connect(
  (state) => ({
    loading: state.loadingState.loading,
  }),
  {hideLoading, showLoading},
)(General);
