import React, {useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  TouchableOpacity,
  TextInput,
  Dimensions,
} from 'react-native';
import {Colors} from '../../../res';
import {sizeWidth} from '../../ultils/SizeUltils';
import {AppStyles} from '../../../res';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {TabView, TabBar} from 'react-native-tab-view';
import GeneralRoute from './components/General';
import TidingsRoute from './components/Tidings';
import EntertainmentRoute from './components/Entertainment';
import SportRoute from './components/Sport';
import EconomicRoute from './components/Economic';
import LawRoute from './components/Law';
import HealthRoute from './components/Health';
import TechnologyRoute from './components/Technology';

const News = () => {
  return (
    <SafeAreaView style={styles.container}>
      <Search />
      <TabViewNews />
    </SafeAreaView>
  );
};

const Search = (props) => {
  const {onChangeText} = props;
  return (
    <View style={styles.headerSearch}>
      <View style={[AppStyles.center]}>
        <View style={styles.containerSearch}>
          <Ionicons
            name={'search'}
            size={20}
            color={Colors.grey70}
            style={[AppStyles.m_horizontal_10]}
          />
          <View style={styles.input}>
            <TextInput
              style={[styles.input, AppStyles.text_input]}
              placeholder={'Tìm kiếm'}
              placeholderTextColor={Colors.grey70}
              onChangeText={onChangeText}
            />
          </View>
        </View>
      </View>
      <TouchableOpacity style={styles.containerBook}>
        <Ionicons name={'book-outline'} size={30} color={Colors.grey70} />
      </TouchableOpacity>
    </View>
  );
};

const TabViewNews = () => {
  const initialLayout = {width: Dimensions.get('window').width, height: 0};

  const [index, setIndex] = useState(0);
  const [routes] = useState([
    {key: 'general', title: 'Tổng hợp'},
    {key: 'tidings', title: 'Thời sự'},
    {key: 'entertainment', title: 'Giải trí'},
    {key: 'sport', title: 'Thể thao'},
    {key: 'economic', title: 'Kinh Tế'},
    {key: 'law', title: 'Pháp luật'},
    {key: 'heath', title: 'Sức khoẻ'},
    {key: 'technology', title: 'Công nghệ'},
  ]);

  const renderScene = ({route}) => {
    switch (route.key) {
      case 'general':
        return <GeneralRoute />;
      case 'tidings':
        return <TidingsRoute />;
      case 'entertainment':
        return <EntertainmentRoute />;
      case 'sport':
        return <SportRoute />;
      case 'economic':
        return <EconomicRoute />;
      case 'law':
        return <LawRoute />;
      case 'heath':
        return <HealthRoute />;
      case 'technology':
        return <TechnologyRoute />;
    }
  };

  return (
    <TabView
      navigationState={{index, routes}}
      renderTabBar={(props) => (
        <TabBar
          {...props}
          indicatorStyle={AppStyles.bg_red}
          tabStyle={AppStyles.w_100}
          scrollEnabled={true}
          style={AppStyles.bg_white}
          getLabelText={({route}) => route.title}
          activeColor={Colors.red}
          inactiveColor={Colors.black}
          labelStyle={styles.textTab}
        />
      )}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={initialLayout}
      swipeEnabled={true}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  headerSearch: {
    height: sizeWidth(50),
    flexDirection: 'row',
  },
  containerSearch: {
    width: sizeWidth(290),
    height: sizeWidth(32),
    backgroundColor: Colors.grey93,
    borderRadius: sizeWidth(15),
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: sizeWidth(1),
    borderColor: Colors.grey89,
  },
  input: {
    width: sizeWidth(230),
    height: sizeWidth(32),
  },
  containerBook: {
    width: '15%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  tabStyle: {
    width: sizeWidth(95),
    height: sizeWidth(30),
  },
  textTab: {
    fontWeight: 'bold',
  },
});

export default News;
