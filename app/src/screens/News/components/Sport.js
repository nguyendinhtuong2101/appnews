import React from 'react';
import {View, StyleSheet, Text} from 'react-native';

const Sport = () => {
  return (
    <View style={styles.container}>
      <Text>Sport</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Sport;
