import React from 'react';
import {View, StyleSheet, Text} from 'react-native';

const Health = () => {
  return (
    <View style={styles.container}>
      <Text>Health</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Health;
