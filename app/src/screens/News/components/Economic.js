import React from 'react';
import {View, StyleSheet, Text} from 'react-native';

const Economic = () => {
  return (
    <View style={styles.container}>
      <Text>Economic</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Economic;
