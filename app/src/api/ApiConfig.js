import {getToken} from '../ultils/Storage';
import axios from 'axios';

// http://api.mediastack.com/v1/news?access_key=248b8c6840c4abc4d736ff204384fcf8&languages=en&countries=vn&limit=10&offset=1
export const API_URL = 'http://localhost:8000/api/';

export const request = async ({method, token = null, data, endpoint}) => {
  return axios({
    method: method || 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'x-access-token': token || (await getToken()),
    },
    data: method === 'POST' ? JSON.stringify(data) : undefined,
    url: `${API_URL}${endpoint}`,
  })
    .then((response) => {
      return response.data;
    })
    .catch((err) => {
      return {errorMessage: err};
    });
};
