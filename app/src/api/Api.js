import {request} from './ApiConfig';

export const getNews = () => {
  return request({
    endpoint:
      'getdata?page=1&limit=5&category=xa-hoi',
  });
};
