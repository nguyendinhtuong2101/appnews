import React from 'react';
import {StyleSheet, View} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {sizeWidth} from '../../ultils/SizeUltils';
import {AppStyles} from '../../../res';

const SkeletonGeneral = () => {
  return (
    <View style={styles.container}>
      <SkeletonPlaceholder>
        <View style={styles.containerButton}>
          <View style={styles.containerImage} />
          <View style={styles.containerText} />
          <View style={styles.containerSource} />
        </View>
      </SkeletonPlaceholder>
      <SkeletonPlaceholder>
        <View style={AppStyles.mg_top_20}>
          <View style={styles.containerButton}>
            <View style={styles.containerImage} />
            <View style={styles.containerText} />
            <View style={styles.containerSource} />
          </View>
        </View>
      </SkeletonPlaceholder>
      <SkeletonPlaceholder>
        <View style={AppStyles.mg_top_20}>
          <View style={styles.containerButton}>
            <View style={styles.containerImage} />
            <View style={styles.containerText} />
            <View style={styles.containerSource} />
          </View>
        </View>
      </SkeletonPlaceholder>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: sizeWidth(10),
  },
  containerButton: {
    width: sizeWidth(355),
    height: sizeWidth(280),
  },
  containerImage: {
    width: sizeWidth(355),
    height: sizeWidth(180),
    borderRadius: sizeWidth(5),
  },
  containerText: {
    width: sizeWidth(355),
    height: sizeWidth(20),
    marginTop: sizeWidth(10),
  },
  containerSource: {
    width: sizeWidth(150),
    height: sizeWidth(20),
    marginTop: sizeWidth(20),
  },
});

export default SkeletonGeneral;
