import moment from 'moment';
import 'moment/locale/vi';

export const fromNow = (date) => {
  let getTime = new Date(date).getTime();
  let checkDateOverWeek = new Date().getTime() - getTime;

  checkDateOverWeek = checkDateOverWeek / 86400000 >= 7;

  if (checkDateOverWeek) {
    return moment(date).format('DD/MM/YYYY');
  }
  moment.locale('vi');
  return moment(date).fromNow();
};
