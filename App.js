import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {navigationRef} from './app/src/router/RootNavigation';
import AppRouter from './app/src/router/AppRouter';
import {Provider} from 'react-redux';
import {store} from './app/src/redux';

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer ref={navigationRef}>
        <AppRouter />
      </NavigationContainer>
    </Provider>
  );
};

export default App;
